package tjamtjam.hwo;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class RaceTestRacingBot extends RacingBot {

	public RaceTestRacingBot(BufferedReader reader, PrintWriter writer, String analyzedTrackFile, double friction)
			throws IOException {
		super(reader, writer);
		String analyzedTrackJson = new Scanner(new File(analyzedTrackFile)).useDelimiter("\\A").next();
		this.track = this.gson.fromJson(analyzedTrackJson, AnalyzedTrack.class);
		this.friction = friction;
		
	}
	
	protected boolean isRaceOn() {
		return true;
	}
	
	protected void analyzeRace() {
		this.trackAnalyzer = new TrackAnalyzer(this.track);
	}

}
