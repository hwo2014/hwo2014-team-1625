package tjamtjam.hwo;

public class AngleAndRadius {

	private double angle;
	private double radius;
	public AngleAndRadius(double value1, double value2) {
		super();
		this.angle = value1;
		this.radius = value2;
	}
	public double getAngle() {
		return angle;
	}
	public double getRadius() {
		return radius;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(angle);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(radius);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AngleAndRadius other = (AngleAndRadius) obj;
		if (Double.doubleToLongBits(angle) != Double
				.doubleToLongBits(other.angle))
			return false;
		if (Double.doubleToLongBits(radius) != Double
				.doubleToLongBits(other.radius))
			return false;
		return true;
	}
	
}
