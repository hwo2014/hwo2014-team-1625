package tjamtjam.hwo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import tjamtjam.hwo.msg.Track.Lane;

public class TrackAnalyzer {

	private static final Logger log = LoggerFactory.getLogger("TA");
	
	public enum AnalyzationMode {
		LANE_SWITCH
	}
	
	public static class AnalyzationResult {
		public final AnalyzationMode mode;
		public final Object data;
		public AnalyzationResult(AnalyzationMode mode, Object data) {
			this.mode = mode;
			this.data = data;
		}
		public String toString() {
			return this.mode+":"+this.data;
		}
	}
	
	private AnalyzedTrack track;
	private AnalyzationRunner runner;
	private List<AnalyzationResult> results;
	private Object monitor = new Object();
	
	public TrackAnalyzer(AnalyzedTrack track) {
		this.track = track;
		this.runner = new AnalyzationRunner();
		this.results = new ArrayList<>();
		new Thread(this.runner).start();
	}
	
	public void stop() {
		this.runner.stop();
		synchronized (this.monitor) {
			this.monitor.notifyAll();
		}
	}
	
	public void analyzeLaneSwitch(int currentPiece, int currentLane) {
		if (!this.runner.analyzing) {
			this.runner.mode = AnalyzationMode.LANE_SWITCH;
			this.runner.currentPiece = currentPiece;
			this.runner.currentLane = currentLane;
			synchronized (this.monitor) {
				this.monitor.notifyAll();
			}
		}
	}
	
	public synchronized List<AnalyzationResult> popResults() {
		if (!this.results.isEmpty()) {
			List<AnalyzationResult> ret = new ArrayList<>(this.results);
			this.results.clear();
			return ret;
		}
		return Collections.emptyList();
	}
	
	public synchronized void addResult(Collection<AnalyzationResult> results) {
		this.results.addAll(results);
	}

	private class AnalyzationRunner implements Runnable {

		private boolean run = true;
		private volatile boolean analyzing = false;
		private volatile AnalyzationMode mode;
		private volatile int currentPiece;
		private volatile int currentLane;
		private volatile double friction;
		
		@Override
		public void run() {
			while (this.run) {
				synchronized (monitor) {
					try {
						log.trace("AnalyzationRunner waiting");
						monitor.wait();
					} catch (InterruptedException e) {}
				}
				this.analyzing = true;
				if (this.mode != null) {
					switch (this.mode) {
					case LANE_SWITCH:
						this.analyzeLaneSwitch();
						break;
					}
				}
				this.analyzing = false;
			}
			log.debug("AnalyzationRunner stopped");
		}
		
		private void analyzeLaneSwitch() {
			log.trace("Analyzing lane switch");
			List<AnalyzedPiece> pieces = track.getAnalyzedPieces();
			
			List<Integer> switchIndexesBefore = new ArrayList<>();
			List<Integer> bendIndexes = new ArrayList<>();
			
			for (int i=0; i<pieces.size(); ++i) {
				int index = i + this.currentPiece;
				if (index >= pieces.size()) {
					index -= pieces.size();
				}
				
				AnalyzedPiece piece = pieces.get(index);
				if (piece.isSwitchPiece()) {
					if (bendIndexes.isEmpty()) {
						switchIndexesBefore.add(index);
					}
					else {
						break;
					}
				}
				else if (piece.isBend()) {
					if (!switchIndexesBefore.isEmpty()) {
						bendIndexes.add(index);
					}
				}
			}
			
			log.trace("switchesBefore: "+switchIndexesBefore+", bend="+bendIndexes);
			
			int numOfLanes = track.getLanes().size();
			int maxLaneDist;
			if (numOfLanes == 2) {
				maxLaneDist = 1;
			}
			else if (numOfLanes == 3) {
				maxLaneDist = this.currentLane == 1 ? 1 : 2;
			}
			else {
				maxLaneDist = (this.currentLane == 0 || this.currentLane == 3) ? 3 : 2;
			}
			
			int preferredLane = -1;
			double currentMinTime = Double.MAX_VALUE;
			for (Lane lane : track.getLanes()) {
				if (Math.abs(lane.getIndex()-this.currentLane) <= maxLaneDist) {
					double laneTime = 0;
					for (Integer index : bendIndexes) {
						AnalyzedPiece piece = pieces.get(index);
						laneTime += piece.getLaneLength(lane.getIndex())*piece.getLaneSpeed(lane.getIndex());
					}
					if (laneTime < currentMinTime) {
						currentMinTime = laneTime;
						preferredLane = lane.getIndex();
					}
				}
			}
			
			List<AnalyzationResult> results = new ArrayList<>();
			int switches = Math.abs(this.currentLane-preferredLane);
			String command = this.currentLane > preferredLane ? "Left" : "Right";
			for (int i=0; i<switches; ++i) {
				results.add(new AnalyzationResult(AnalyzationMode.LANE_SWITCH, command));
			}
			log.trace("Resolved lane switches: "+results+", currentLane="+this.currentLane+", preferred="+preferredLane);
			addResult(results);
		}
		
		public void stop() {
			this.run = false;
		}
		
	}
	
}
