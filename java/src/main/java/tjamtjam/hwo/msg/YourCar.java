package tjamtjam.hwo.msg;

public class YourCar {

	private String msgType;
	private CarId data;
	
	public String getMsgType() {
		return msgType;
	}
	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}
	public CarId getData() {
		return data;
	}
	public void setData(CarId data) {
		this.data = data;
	}
}
