package tjamtjam.hwo.msg;

import java.util.ArrayList;
import java.util.List;

public class CarPositions {

	private String msgType;
	private List<CarPosition> data;
	private String gameId;
	private int gameTick;
	
	public String getMsgType() {
		return msgType;
	}
	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}
	public List<CarPosition> getData() {
		return data;
	}
	public void setData(List<CarPosition> data) {
		if (this.data == null) {
			this.data = new ArrayList<>();
		}
		this.data = data;
	}
	public String getGameId() {
		return gameId;
	}
	public void setGameId(String gameId) {
		this.gameId = gameId;
	}
	public int getGameTick() {
		return gameTick;
	}
	public void setGameTick(int gameTick) {
		this.gameTick = gameTick;
	}
	@Override
	public String toString() {
		return "CarPositions [msgType=" + msgType + ", data=" + data
				+ ", gameId=" + gameId + ", gameTick=" + gameTick + "]";
	}
	
}
