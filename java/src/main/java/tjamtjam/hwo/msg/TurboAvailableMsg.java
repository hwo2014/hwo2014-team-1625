package tjamtjam.hwo.msg;

public class TurboAvailableMsg {

	private TurboData data;
	
	public double getTurboDurationMilliseconds() {
		return data.turboDurationMilliseconds;
	}
	public int getTurboDurationTicks() {
		if (data.turboDurationTicks == 0) {
			return (int)(data.turboDurationMilliseconds * 0.06);
		}
		return data.turboDurationTicks;
	}
	public double getTurboFactor() {
		return data.turboFactor;
	}
	
	public class TurboData {
		private double turboDurationMilliseconds;
		private int turboDurationTicks;
		private double turboFactor;
	}
	
}
