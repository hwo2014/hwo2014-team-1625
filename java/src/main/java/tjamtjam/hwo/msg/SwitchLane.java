package tjamtjam.hwo.msg;

public class SwitchLane extends SendMsg {

	private String direction;
	
	public SwitchLane(String direction) {
		this.direction = direction;
	}
	
	@Override
    protected Object msgData() {
        return this.direction;
    }
	
	@Override
	protected String msgType() {
		return "switchLane";
	}

	
}
