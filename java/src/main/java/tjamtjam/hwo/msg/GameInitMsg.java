package tjamtjam.hwo.msg;

public class GameInitMsg {

	private RaceWrapper data;
	
	public Race getRace() {
		return this.data.race;
	}
	
	public class RaceWrapper {
		private Race race;
	}
}
