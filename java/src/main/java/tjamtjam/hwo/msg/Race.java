package tjamtjam.hwo.msg;

import java.util.ArrayList;
import java.util.List;

public class Race {

	private Track track;
	private List<CarInfo> cars;
	private RaceSession raceSession;
	
	public Track getTrack() {
		return track;
	}

	public void setTrack(Track track) {
		this.track = track;
	}

	public List<CarInfo> getCars() {
		if (this.cars == null) {
			this.cars = new ArrayList<>();
		}
		return cars;
	}

	public void setCars(List<CarInfo> cars) {
		this.cars = cars;
	}

	public RaceSession getRaceSession() {
		return raceSession;
	}

	public void setRaceSession(RaceSession raceSession) {
		this.raceSession = raceSession;
	}

	public class RaceSession {
		private int laps;
		private long maxLapTimeMs;
		private boolean quickRace;
		private long durationMs;
		public int getLaps() {
			return laps;
		}
		public void setLaps(int laps) {
			this.laps = laps;
		}
		public long getMaxLapTimeMs() {
			return maxLapTimeMs;
		}
		public void setMaxLapTimeMs(long maxLapTimeMs) {
			this.maxLapTimeMs = maxLapTimeMs;
		}
		public boolean isQuickRace() {
			return quickRace;
		}
		public void setQuickRace(boolean quickRace) {
			this.quickRace = quickRace;
		}
		public long getDurationMs() {
			return durationMs;
		}
	}
}
