package tjamtjam.hwo.msg;

public class JoinRace extends SendMsg {

	private Join botId;
	private String trackName;
	private String password;
	private int carCount;
	
	public JoinRace(String name, String key, String trackName, String password, int carCount) {
		this.botId = new Join(name, key);
		this.trackName = trackName;
		this.password = password;
		this.carCount = carCount;
	}
	
	@Override
	protected String msgType() {
		return "joinRace";
	}
	
}
