package tjamtjam.hwo.msg;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.SerializedName;

public class Track {

	private String id;
	private String name;
	private List<TrackPiece> pieces;
	private List<Lane> lanes;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<TrackPiece> getPieces() {
		if (this.pieces == null) {
			this.pieces = new ArrayList<>();
		}
		return pieces;
	}

	public void setPieces(List<TrackPiece> pieces) {
		this.pieces = pieces;
	}

	public List<Lane> getLanes() {
		if (this.lanes == null) {
			this.lanes = new ArrayList<>();
		}
		return lanes;
	}

	public void setLanes(List<Lane> lanes) {
		this.lanes = lanes;
	}

	public static class TrackPiece {
		private double length;
		private double radius;
		private double angle;
		@SerializedName("switch")
		private boolean switchPiece;
		public double getLength() {
			return length;
		}
		public void setLength(double length) {
			this.length = length;
		}
		public double getRadius() {
			return radius;
		}
		public void setRadius(double radius) {
			this.radius = radius;
		}
		public double getAngle() {
			return angle;
		}
		public void setAngle(double angle) {
			this.angle = angle;
		}
		public boolean isSwitchPiece() {
			return switchPiece;
		}
		public void setSwitchPiece(boolean switchPiece) {
			this.switchPiece = switchPiece;
		}
		public boolean isBend() {
			return this.angle != 0;
		}
		@Override
		public String toString() {
			return "TrackPiece [length=" + length + ", radius=" + radius
					+ ", angle=" + angle + ", switchPiece=" + switchPiece + "]";
		}
	}
	
	public static class Lane {
		private int index;
		private double distanceFromCenter;
		public int getIndex() {
			return index;
		}
		public void setIndex(int index) {
			this.index = index;
		}
		public double getDistanceFromCenter() {
			return distanceFromCenter;
		}
		public void setDistanceFromCenter(double distanceFromCenter) {
			this.distanceFromCenter = distanceFromCenter;
		}
		@Override
		public String toString() {
			return "Lane [index=" + index + ", distanceFromCenter="
					+ distanceFromCenter + "]";
		}
		
	}

	@Override
	public String toString() {
		return "Track [id=" + id + ", name=" + name + ", pieces=" + pieces
				+ ", lanes=" + lanes + "]";
	}
}
