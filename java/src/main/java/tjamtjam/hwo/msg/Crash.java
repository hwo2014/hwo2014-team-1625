package tjamtjam.hwo.msg;

public class Crash {

	protected String gameId;
	protected int gameTick;
	protected CarId data;
	public String getGameId() {
		return gameId;
	}
	public int getGameTick() {
		return gameTick;
	}
	public CarId getData() {
		return data;
	}
	
}
