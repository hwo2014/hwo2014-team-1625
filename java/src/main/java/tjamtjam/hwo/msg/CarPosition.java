package tjamtjam.hwo.msg;

public class CarPosition {

	private double angle;
	private CarId id;
	private PiecePosition piecePosition;
	
	public double getAngle() {
		return angle;
	}

	public void setAngle(double angle) {
		this.angle = angle;
	}

	public CarId getId() {
		return id;
	}

	public void setId(CarId id) {
		this.id = id;
	}

	public PiecePosition getPiecePosition() {
		return piecePosition;
	}

	public void setPiecePosition(PiecePosition piecePosition) {
		this.piecePosition = piecePosition;
	}
	
	public String toString() {
		return this.id+"={"+this.piecePosition+", "+this.angle+"}";
	}

	public class PiecePosition {
		private int pieceIndex;
		private double inPieceDistance;
		private int lap;
		private Lane lane;
		public int getPieceIndex() {
			return pieceIndex;
		}
		public void setPieceIndex(int pieceIndex) {
			this.pieceIndex = pieceIndex;
		}
		public double getInPieceDistance() {
			return inPieceDistance;
		}
		public void setInPieceDistance(double inPieceDistance) {
			this.inPieceDistance = inPieceDistance;
		}
		public int getLap() {
			return lap;
		}
		public void setLap(int lap) {
			this.lap = lap;
		}
		public Lane getLane() {
			return lane;
		}
		public void setLane(Lane lane) {
			this.lane = lane;
		}
		@Override
		public String toString() {
			return "Pos:(idx="+this.pieceIndex+", dist="+this.inPieceDistance+", lap="+this.lap+", lane="+this.lane+")";
		}
	}
	
	public class Lane {
		private int startLaneIndex;
		private int endLaneIndex;
		public int getStartLaneIndex() {
			return startLaneIndex;
		}
		public void setStartLaneIndex(int startLaneIndex) {
			this.startLaneIndex = startLaneIndex;
		}
		public int getEndLaneIndex() {
			return endLaneIndex;
		}
		public void setEndLaneIndex(int endLaneIndex) {
			this.endLaneIndex = endLaneIndex;
		}
		@Override
		public String toString() {
			return "["+this.startLaneIndex+", "+this.endLaneIndex+"]";
		}
	}
}


