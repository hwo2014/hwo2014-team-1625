package tjamtjam.hwo.msg;

public class LapFinished {

	private LapFinishedData data;
	private String gameId;
	private int gameTick;
	
	public LapFinishedData getData() {
		return data;
	}

	public String getGameId() {
		return gameId;
	}

	public int getGameTick() {
		return gameTick;
	}

	public class LapFinishedData {
		private CarId car;
		private LapTime lapTime;
		private RaceTime raceTime;
		private Ranking ranking;
		public CarId getCar() {
			return car;
		}
		public LapTime getLapTime() {
			return lapTime;
		}
		public RaceTime getRaceTime() {
			return raceTime;
		}
		public Ranking getRanking() {
			return ranking;
		}
	}
	
	public class LapTime {
		private int lap;
		private int ticks;
		private long millis;
		public int getLap() {
			return lap;
		}
		public int getTicks() {
			return ticks;
		}
		public long getMillis() {
			return millis;
		}
	}
	
	public class RaceTime {
		private int laps;
		private int ticks;
		private long millis;
		public int getLaps() {
			return laps;
		}
		public int getTicks() {
			return ticks;
		}
		public long getMillis() {
			return millis;
		}
	}
	
	public class Ranking {
		private int overall;
		private int fastestLap;
		public int getOverall() {
			return overall;
		}
		public int getFastestLap() {
			return fastestLap;
		}
	}
	
}
