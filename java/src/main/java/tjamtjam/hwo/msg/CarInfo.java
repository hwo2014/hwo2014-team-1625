package tjamtjam.hwo.msg;

public class CarInfo {

	private CarId id;
	private CarDimensions dimensions;
	
	public CarId getId() {
		return id;
	}

	public void setId(CarId id) {
		this.id = id;
	}

	public CarDimensions getDimensions() {
		return dimensions;
	}

	public void setDimensions(CarDimensions dimensions) {
		this.dimensions = dimensions;
	}
	
	@Override
	public String toString() {
		return this.id+"={"+this.dimensions+"}";
	}

	public class CarDimensions {
		private double length;
		private double width;
		private double guideFlagPositions;
		public double getLength() {
			return length;
		}
		public void setLength(double length) {
			this.length = length;
		}
		public double getWidth() {
			return width;
		}
		public void setWidth(double width) {
			this.width = width;
		}
		public double getGuideFlagPositions() {
			return guideFlagPositions;
		}
		public void setGuideFlagPositions(double guideFlagPositions) {
			this.guideFlagPositions = guideFlagPositions;
		}
		@Override
		public String toString() {
			return "Dim:(length="+this.length+", width="+this.width+", pos="+this.guideFlagPositions+")";
		}
	}
}
