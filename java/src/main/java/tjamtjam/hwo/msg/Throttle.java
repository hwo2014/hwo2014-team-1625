package tjamtjam.hwo.msg;

public class Throttle extends SendMsg {
    private double value;
    private int gameTick;

    public Throttle(double value, int gameTick) {
        this.value = value;
        this.gameTick = gameTick;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "throttle";
    }
}