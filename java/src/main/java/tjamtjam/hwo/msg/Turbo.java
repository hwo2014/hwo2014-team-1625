package tjamtjam.hwo.msg;

public class Turbo extends SendMsg {

	@Override
	protected String msgType() {
		return "turbo";
	}

	@Override
	protected Object msgData() {
		return "MOAR POWER!!";
	}

}
