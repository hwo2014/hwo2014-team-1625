package tjamtjam.hwo.msg;

public class Ping extends SendMsg {
    @Override
    protected String msgType() {
        return "ping";
    }
}