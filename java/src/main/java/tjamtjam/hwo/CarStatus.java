package tjamtjam.hwo;

import tjamtjam.hwo.msg.CarPosition;

public class CarStatus {

	private int pieceIndex;
	private double angle;
	private double pieceDistance;
	private double speed;
	private int tick;
	private int lane;
	
	public CarStatus(CarPosition pos, int tick) {
		this.pieceIndex = pos.getPiecePosition().getPieceIndex();
		this.pieceDistance = pos.getPiecePosition().getInPieceDistance();
		this.lane = pos.getPiecePosition().getLane().getEndLaneIndex();
		this.angle = pos.getAngle();
		this.tick = tick;
	}
	
	public int getPieceIndex() {
		return pieceIndex;
	}
	public double getAngle() {
		return angle;
	}
	public double getPieceDistance() {
		return pieceDistance;
	}
	public double getSpeed() {
		return speed;
	}
	public void computeSpeed(int prevTick, double distance) {
		this.speed = distance/(double)(this.tick - prevTick);
	}

	public int getLane() {
		return lane;
	}

	public int getTick() {
		return tick;
	}
	
	@Override
	public String toString() {
		return "["+this.tick+"] "+this.pieceIndex+" "+this.speed+" "+this.angle+" "+this.lane;
	}
}
