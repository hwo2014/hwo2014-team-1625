package tjamtjam.hwo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import tjamtjam.hwo.msg.Track;

public class AnalyzedTrack extends Track {

	private List<AnalyzedPiece> analyzedPieces;
	private double[] totalLaneLengths;
	private double totalLength;
	
	public AnalyzedTrack(Track track) {
		this.setId(track.getId());
		this.setName(track.getName());
		this.setLanes(track.getLanes());
		this.setPieces(track.getPieces());
		this.totalLength = 0;
		this.totalLaneLengths = new double[this.getLanes().size()];
		this.analyzedPieces = new ArrayList<>(this.getPieces().size());
		
		for (TrackPiece piece : this.getPieces()) {
			AnalyzedPiece aPiece = new AnalyzedPiece(piece, this.getLanes());
			for (Lane lane : this.getLanes()) {
				this.totalLaneLengths[lane.getIndex()] += aPiece.getLaneLength(lane.getIndex());
			}
			this.analyzedPieces.add(aPiece);
			if (piece.isBend()) {
				this.totalLength += Math.abs(piece.getRadius()*Math.PI*piece.getAngle()/180d);
			}
			else {
				this.totalLength += piece.getLength();
			}
		}
		
	}
	
	public double getTotalLaneLength(int index) {
		return this.totalLaneLengths[index];
	}
	
	public AnalyzedPiece getAnalyzedPiece(int index) {
		return this.analyzedPieces.get(index);
	}
	
	public List<AnalyzedPiece> getAnalyzedPieces() {
		return this.analyzedPieces;
	}

	public double getTotalLength() {
		return totalLength;
	}

	@Override
	public String toString() {
		return "AnalyzedTrack [analyzedPieces=" + analyzedPieces
				+ ", totalLaneLengths=" + Arrays.toString(totalLaneLengths)
				+ "]";
	}
}
