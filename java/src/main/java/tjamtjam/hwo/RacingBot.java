package tjamtjam.hwo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.LoggerContext;

import com.google.gson.Gson;

import tjamtjam.hwo.TrackAnalyzer.AnalyzationMode;
import tjamtjam.hwo.TrackAnalyzer.AnalyzationResult;
import tjamtjam.hwo.msg.CarId;
import tjamtjam.hwo.msg.CarPosition;
import tjamtjam.hwo.msg.CarPositions;
import tjamtjam.hwo.msg.Crash;
import tjamtjam.hwo.msg.GameInitMsg;
import tjamtjam.hwo.msg.Join;
import tjamtjam.hwo.msg.JoinRace;
import tjamtjam.hwo.msg.LapFinished;
import tjamtjam.hwo.msg.MsgWrapper;
import tjamtjam.hwo.msg.Ping;
import tjamtjam.hwo.msg.Race;
import tjamtjam.hwo.msg.SendMsg;
import tjamtjam.hwo.msg.Spawn;
import tjamtjam.hwo.msg.SwitchLane;
import tjamtjam.hwo.msg.Throttle;
import tjamtjam.hwo.msg.Turbo;
import tjamtjam.hwo.msg.TurboAvailableMsg;
import tjamtjam.hwo.msg.TurboChangeMsg;
import tjamtjam.hwo.msg.YourCar;

public class RacingBot {

	private static Logger log = LoggerFactory.getLogger("RB");
	
	private static final double EPSILON = 0.001;
	public static final double GRAVITY = 1;
	
	private static final int FIRST_ANALYZE_TICK = 20;
	private static final int SECOND_ANALYZE_TICK = 40;
	private static double ANGLE_LIMIT = 54;
	private static double ANGLE_DIFF_ANGLE_LIMIT = ANGLE_LIMIT - 10;
	private static double ANGLE_DIFF_LIMIT = 2;
	private static double TURBO_STRAIGHT_LENGTH = 300;
	
	private PrintWriter writer;
	private BufferedReader reader;
	protected final Gson gson = new Gson();
	private String myCarColor;
	private Race race;
	protected AnalyzedTrack track;
	private CarStatus prevStatus;
	private CarStatus myCarStatus;
	private CarStatus prevPrintedStatus;
	protected TrackAnalyzer trackAnalyzer;
	private Deque<AnalyzationResult> laneSwitches;
	private boolean laneSwitchSent = false;
	private int laneSwitchIndex = -1;
	private int turboTicks = 0;
	private double turboFactor = 1;
	private boolean turboOn = false;
	private boolean sendTurbo = false;
	private int currentLap = 0;
	
	private Map<String, CarStatus> opponentStatuses = new HashMap<>();
	private Map<String, CarStatus> prevOpponentStatuses = new HashMap<>();
	private Set<String> crashedOpponents = new HashSet<>();
	private AnalyzationResult opponentLaneSwitch = null;
	private boolean opponentLaneSwitchSent = false;
	
	private int bendStartIndex = -1;
	private double maxAngle = 0;
	private boolean skipBendAdjust = false;
	private int crashBendStart = -1;
	private int crashBendEnd = -1;
	
	private double velocity0;
	private double velocity1;
	private double distance;
	protected double friction = -1;
	
	private long startTime;
	private long computingTime = 0;
	
	public RacingBot(final BufferedReader reader, final PrintWriter writer) throws IOException {
		this.writer = writer;
		this.reader = reader;
		this.laneSwitches = new LinkedList<>();
	}
	
	public static void main(String... args) throws IOException {
		
		ch.qos.logback.classic.Logger root = (ch.qos.logback.classic.Logger)LoggerFactory.getLogger(ch.qos.logback.classic.Logger.ROOT_LOGGER_NAME);
		root.setLevel(Level.INFO);
		
		String host = args[0];
		int port = Integer.parseInt(args[1]);
		String botName = args[2];
		String botKey = args[3];
		String trackName = null;
		if (args.length > 4) {
			trackName = args[4];
		}
		String password = null;
		int players = 1;
		boolean playersRead = false;
		String analyzedFile = null;
		if (args.length > 5) {
			try {
				players = Integer.parseInt(args[5]);
				playersRead = true;
			} catch (NumberFormatException e) {
				password = args[5];
			}
		}
		if (args.length > 6) {
			if (playersRead) {
				analyzedFile = args[6];
			}
			else {
				try {
					players = Integer.parseInt(args[6]);
				} catch (NumberFormatException e) {
				}
			}
		}

		log.info("Connecting to " + host + ":" + port + " as "+ botName + "/" + botKey);

		final Socket socket = new Socket(host, port);
		final PrintWriter writer = new PrintWriter(new OutputStreamWriter( socket.getOutputStream(), "utf-8"));

		final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

		RacingBot bot;
		if (analyzedFile != null) {
			bot = new RaceTestRacingBot(reader, writer, analyzedFile, Double.parseDouble(args[args.length -1]));
		}
		else {
			bot = new RacingBot(reader, writer);
		}
		if (trackName == null || trackName.trim().length() == 0) {
			bot.startAndRun(new Join(botName, botKey));
		}
		else {
			bot.startAndRun(new JoinRace(botName, botKey, trackName, password, players));
		}
		
		log.trace(new Gson().toJson(bot.track));
	}
	
	public void startAndRun(final SendMsg join) throws IOException {
		this.send(join);
		String line;
		while((line = reader.readLine()) != null) {
			long start = System.currentTimeMillis();
			MsgWrapper msg = this.gson.fromJson(line, MsgWrapper.class);
			if ("carPositions".equals(msg.msgType)) {
				CarPositions positions = this.gson.fromJson(line, CarPositions.class);
				this.handleCarPositions(positions);
			}
			else if ("yourCar".equals(msg.msgType)) {
				YourCar car = this.gson.fromJson(line, YourCar.class);
				if (car.getData() != null) {
					this.myCarColor = car.getData().getColor();
					log.info("My car got color: "+this.myCarColor);
				}
				else {
					log.error("yourCar message did not contain data! line="+line);
				}
			}
			else if ("gameInit".equals(msg.msgType)) {
				GameInitMsg gameInit = this.gson.fromJson(line, GameInitMsg.class);
				if (this.race == null) {
					this.race = gameInit.getRace();
					this.analyzeRace();
					log.info("Game initialized: "+line);
				}
				else {
					if (gameInit != null && gameInit.getRace() != null && gameInit.getRace().getRaceSession() != null) {
						this.race.setRaceSession(gameInit.getRace().getRaceSession());
					}
					log.info("Game already initialized. Race session updated.");
				}
//				if (this.isRaceOn()) {
//					ANGLE_LIMIT = 0.95*ANGLE_LIMIT;
//					ANGLE_DIFF_ANGLE_LIMIT = ANGLE_LIMIT - 10;
//					log.info("Updated ANGLE_LIMIT to "+ANGLE_LIMIT);
//				}
			}
			else if ("crash".equals(msg.msgType)) {
				Crash crash = this.gson.fromJson(line, Crash.class);
				if (this.myCarColor.equals(crash.getData().getColor())) {
					log.info(this.myCarStatus+" CRASH!!!");
					this.turboOn = false;
					this.adjustBendSpeedAfterCrash();
				}
				else {
					log.info("["+this.myCarStatus.getTick()+"] Opponent "+crash.getData()+" crashed!");
					this.crashedOpponents.add(crash.getData().getColor());
				}
				this.send(new Ping());
			}
			else if ("spawn".equals(msg.msgType)) {
				Spawn spawn = this.gson.fromJson(line, Spawn.class);
				log.info("["+this.myCarStatus.getTick()+"] "+spawn.getData()+" spawned");
				this.crashedOpponents.remove(spawn.getData().getColor());
			}
			else if ("turboAvailable".equals(msg.msgType)) {
				TurboAvailableMsg turbo = this.gson.fromJson(line, TurboAvailableMsg.class);
				this.turboTicks = turbo.getTurboDurationTicks();
				this.turboFactor = turbo.getTurboFactor();
				log.info("["+this.myCarStatus.getTick()+"] TURBO available! ticks="+this.turboTicks);
			}
			else if ("turboStart".equals(msg.msgType)) {
				TurboChangeMsg turboMsg = this.gson.fromJson(line, TurboChangeMsg.class);
				if (this.myCarColor.equals(turboMsg.getData().getColor())) {
					this.turboOn = true;
					log.info("["+this.myCarStatus.getTick()+"] TURBO started!");
				}
			}
			else if ("turboEnd".equals(msg.msgType)) {
				TurboChangeMsg turboMsg = this.gson.fromJson(line, TurboChangeMsg.class);
				if (this.myCarColor.equals(turboMsg.getData().getColor())) {
					this.turboOn = false;
					log.info("["+this.myCarStatus.getTick()+"] TURBO ended!");
				}
			}
			else if ("lapFinished".equals(msg.msgType)) {
				LapFinished lap = this.gson.fromJson(line, LapFinished.class);
				CarId car = lap.getData().getCar();
				if (this.myCarColor.equals(car.getColor())) {
					this.currentLap = lap.getData().getRaceTime().getLaps() + 1;
					log.info("Lap finished. currentLap="+this.currentLap+", lastLap="+(this.currentLap >= this.race.getRaceSession().getLaps()));
				}
			}
			else if ("gameStart".equals(msg.msgType)) {
				log.info("START!");
				this.send(new Throttle(1.0, 0));
			}
			else if ("gameEnd".equals(msg.msgType)) {
				this.myCarStatus = null;
				this.prevStatus = null;
				this.maxAngle = 0;
				this.bendStartIndex = -1;
				this.crashBendStart = -1;
				this.crashBendEnd = -1;
				this.turboOn = false;
				this.laneSwitchSent = false;
				this.laneSwitches.clear();
				log.info("Game END! "+line);
			}
			else if ("tournamentEnd".equals(msg.msgType)) {
				log.info("Tournament END! "+line);
				this.trackAnalyzer.stop();
			}
			else if ("finish".equals(msg.msgType)) {
				log.info("Finish message received: "+line);
				this.send(new Ping());
			}
			else if ("dnf".equals(msg.msgType)) {
				log.info("DNF message received: "+line);
				this.send(new Ping());
			}
			else {
				log.info("Unhandled message: "+line);
			}
			this.computingTime += System.currentTimeMillis()-start;
		}
		
	}
	
	private void handleCarPositions(CarPositions positions) {
		if (this.startTime == 0) {
			this.startTime = System.currentTimeMillis();
		}
		
		double throttle = -1;
		this.analyzePositions(positions);
		if (this.sendTurbo) {
			log.info("["+this.myCarStatus.getTick()+"] Sending TURBO message!");
			this.send(new Turbo());
			this.sendTurbo = false;
			this.turboTicks = 0;
		}
		else if (this.opponentLaneSwitch != null && !this.opponentLaneSwitchSent) {
			log.info("["+this.myCarStatus.getTick()+"] Sending "+this.opponentLaneSwitch+" for overtaking!");
			this.send(new SwitchLane((String)this.opponentLaneSwitch.data));
			this.opponentLaneSwitchSent = true;
		}
		else if (!this.laneSwitchSent && !this.laneSwitches.isEmpty()) {
			AnalyzationResult result = this.laneSwitches.poll();
			log.info("["+this.myCarStatus.getTick()+"] Sending "+result+", laneSwitches="+this.laneSwitches);
			this.send(new SwitchLane((String)result.data));
			this.laneSwitchSent = true;
		}
		else {
			if (!this.isRaceOn() && positions.getGameTick() < FIRST_ANALYZE_TICK) {
				throttle = 0.75;
			}
			else if (!this.isRaceOn() && positions.getGameTick() < SECOND_ANALYZE_TICK) {
				throttle = 0d;
			}
			else {
				throttle = this.resolveThrottle();
			}
			this.send(new Throttle(throttle, positions.getGameTick()));
		}
		if (statusChanged(this.prevPrintedStatus, this.myCarStatus)) {
			AnalyzedPiece piece = this.track.getAnalyzedPiece(this.myCarStatus.getPieceIndex());
			double angleDiff = this.prevStatus != null ? Math.abs(this.myCarStatus.getAngle())-Math.abs(this.prevStatus.getAngle()) : 0;
			String msg = this.myCarStatus+" ad="+angleDiff+" t="+
					throttle+" ls="+piece.getLaneSpeed(this.myCarStatus.getLane())+" ols="+this.opponentLaneSwitch+"("+this.opponentLaneSwitchSent+") "+
					(double)positions.getGameTick()/((System.currentTimeMillis()-this.startTime)/1000)+" "+
					(double)positions.getGameTick()/((double)this.computingTime/1000);
			if (positions.getGameTick() % 10 == 0) {
				this.prevPrintedStatus = this.myCarStatus;
				log.info(msg);
			}
			else if (log.isDebugEnabled()) {
				this.prevPrintedStatus = this.myCarStatus;
				log.debug(msg);
			}
		}
	}
	
	protected void analyzeRace() {
		this.track = new AnalyzedTrack(this.race.getTrack());
		this.trackAnalyzer = new TrackAnalyzer(this.track);
	}
	
	private void analyzePositions(CarPositions positions) {
		CarPosition myPos = this.findMyPosition(positions);
		if (myPos != null) {
			CarStatus newStatus = new CarStatus(myPos, positions.getGameTick());
			if (this.myCarStatus != null) {
				double distance;
				if (newStatus.getPieceIndex() == this.myCarStatus.getPieceIndex()) {
					distance = newStatus.getPieceDistance()-this.myCarStatus.getPieceDistance();
				}
				else {
					AnalyzedPiece prevPiece = this.track.getAnalyzedPiece(this.myCarStatus.getPieceIndex());
					distance = prevPiece.getLaneLength(this.myCarStatus.getLane())-this.myCarStatus.getPieceDistance()+newStatus.getPieceDistance();
				}
				newStatus.computeSpeed(this.myCarStatus.getTick(), distance);
				if (this.laneSwitchSent && newStatus.getLane() != this.myCarStatus.getLane()) {
					log.info("["+newStatus.getTick()+"] Switched lane: "+this.myCarStatus.getLane()+"->"+newStatus.getLane());
					this.laneSwitchIndex = newStatus.getPieceIndex();
					this.laneSwitchSent = false;
					this.opponentLaneSwitch = null;
					this.opponentLaneSwitchSent = false;
				}
				if (!this.isRaceOn()) {
					if (positions.getGameTick() == FIRST_ANALYZE_TICK) {
						this.velocity0 = newStatus.getSpeed();
					}
					else if (positions.getGameTick() >= FIRST_ANALYZE_TICK && positions.getGameTick() < SECOND_ANALYZE_TICK) {
						this.distance += distance;
					}
					else if (positions.getGameTick() == SECOND_ANALYZE_TICK) {
						this.velocity1 = newStatus.getSpeed();
						this.friction = (this.velocity0*this.velocity0 - this.velocity1*this.velocity1)/(2*GRAVITY*this.distance);
						log.info("Resolved friction: "+this.friction+" v0="+this.velocity0+", v1="+this.velocity1+" d="+this.distance);
					}
				}
			}
			this.prevStatus = this.myCarStatus;
			this.myCarStatus = newStatus;
			
			if (this.myCarStatus != null && this.prevStatus != null) {
				AnalyzedPiece prevPiece = this.track.getAnalyzedPiece(this.prevStatus.getPieceIndex());
				AnalyzedPiece currPiece = this.track.getAnalyzedPiece(this.myCarStatus.getPieceIndex());
				if (prevPiece.isSwitchPiece() && !currPiece.isSwitchPiece()) {
					if ((this.laneSwitchSent || this.opponentLaneSwitchSent) && this.prevStatus.getLane() == this.myCarStatus.getLane() && this.prevStatus.getPieceIndex() != this.laneSwitchIndex) {
						log.info("["+this.myCarStatus.getTick()+"] Lane switch did not succeed at "+this.prevStatus.getPieceIndex()+"!");
						this.laneSwitchSent = false;
						this.opponentLaneSwitch = null;
						this.opponentLaneSwitchSent = false;
					}
					this.laneSwitchIndex = -1;
				}
			}
			
			this.analyzeOpponenPositions(positions);
			
			if (this.opponentLaneSwitch == null) {
				List<AnalyzationResult> results = this.trackAnalyzer.popResults();
				if (!results.isEmpty()) {
					AnalyzationResult first = results.get(0);
					if (first.mode == AnalyzationMode.LANE_SWITCH) {
						this.laneSwitches.addAll(results);
					}
				}
				if (!this.laneSwitchSent && this.laneSwitches.isEmpty()) {
					if (results.isEmpty()) {
						this.trackAnalyzer.analyzeLaneSwitch(this.myCarStatus.getPieceIndex(), this.myCarStatus.getLane());
					}
				}
			}
			else if (!this.opponentLaneSwitchSent) {
				this.laneSwitches.clear();
				this.laneSwitchSent = false;
			}
			this.adjustBendSpeed();
		}
		else {
			log.warn("Could not find my position!");
		}
	}
	
	private void analyzeOpponenPositions(CarPositions positions) {
		Map<Integer, Collection<String>> indexToOpponentColor = new HashMap<>();
		Map<String, Double> opponentDistances = new HashMap<>();
		Map<String, CarStatus> newStatuses = new HashMap<>();
		for (CarPosition position : positions.getData()) {
			CarId id = position.getId();
			if (!this.myCarColor.equals(id.getColor())) {
				CarStatus newStatus = new CarStatus(position, positions.getGameTick());
				newStatuses.put(id.getColor(), newStatus);
				CarStatus prevStatus = this.opponentStatuses.get(id.getColor());
				if (prevStatus != null) {
					double distance;
					if (newStatus.getPieceIndex() == prevStatus.getPieceIndex()) {
						distance = newStatus.getPieceDistance()-prevStatus.getPieceDistance();
					}
					else {
						AnalyzedPiece prevPiece = this.track.getAnalyzedPiece(prevStatus.getPieceIndex());
						distance = prevPiece.getLaneLength(prevStatus.getLane())-prevStatus.getPieceDistance()+newStatus.getPieceDistance();
					}
					newStatus.computeSpeed(prevStatus.getTick(), distance);
					if (newStatus.getLane() != prevStatus.getLane()) {
						log.debug("["+this.myCarStatus.getTick()+"] Opponent "+id.getColor()+" switched lanes! "+
								prevStatus.getLane()+"("+prevStatus.getTick()+")->"+newStatus.getLane()+"("+newStatus.getTick()+")");
						this.opponentLaneSwitch = null;
					}
				}
				Collection<String> indexOpponents = indexToOpponentColor.get(newStatus.getPieceIndex());
				if (indexOpponents == null) {
					indexOpponents = new ArrayList<>();
				}
				indexOpponents.add(id.getColor());
				indexToOpponentColor.put(newStatus.getPieceIndex(), indexOpponents);
			}
		}
		this.prevOpponentStatuses = this.opponentStatuses;
		this.opponentStatuses = newStatuses;
		
		double distance = 0;
		List<AnalyzedPiece> pieces = this.track.getAnalyzedPieces();
		for (int i=0; i<pieces.size(); ++i) {
			int index = i + this.myCarStatus.getPieceIndex();
			if (index >= pieces.size()) {
				index -= pieces.size();
			}
			AnalyzedPiece piece = pieces.get(index);
			
			double pieceLength;
			
			if (piece.isBend()) {
				pieceLength = Math.abs(piece.getRadius()*Math.PI*piece.getAngle()/180d);
			}
			else {
				pieceLength = piece.getLength();
			}
			if (i == 0) {
				double myPieceDistance;
				if (piece.isBend()) {
					double angle = (180d*this.myCarStatus.getPieceDistance())/(Math.PI*piece.getLaneRadius(this.myCarStatus.getLane()));
					myPieceDistance = Math.abs(piece.getRadius()*Math.PI*angle/180d);
				}
				else {
					myPieceDistance = this.myCarStatus.getPieceDistance();
				}
				distance += pieceLength - myPieceDistance;
			}
			else {
				distance += pieceLength;
			}
			
			Collection<String> indexOpponents = indexToOpponentColor.get(index);
			if (indexOpponents != null) {
				for (String opponentColor : indexOpponents) {
					CarStatus status = this.opponentStatuses.get(opponentColor);
					double opponentDistance;
					if (piece.isBend()) {
						double angle = (180d*status.getPieceDistance())/(Math.PI*piece.getLaneRadius(status.getLane()));
						double pieceDistance = Math.abs(piece.getRadius()*Math.PI*angle/180d);
						opponentDistance = distance-(pieceLength-pieceDistance);
					}
					else {
						opponentDistance = distance-(pieceLength-status.getPieceDistance());
					}
					if (opponentDistance > this.track.getTotalLength()/2) {
						opponentDistance = opponentDistance - this.track.getTotalLength();
					}
					opponentDistances.put(opponentColor, opponentDistance);
				}
			}
			
		}
		
		if (this.opponentLaneSwitch == null) {
			for (String opponentColor : opponentDistances.keySet()) {
				double oppDistance = opponentDistances.get(opponentColor);
				if (oppDistance >= 0d && oppDistance < 100d && !this.crashedOpponents.contains(opponentColor)) {
					CarStatus opponentStatus = this.opponentStatuses.get(opponentColor);
					if (opponentStatus.getLane() == this.myCarStatus.getLane()) {
						int maxLane = this.track.getLanes().size()-1;
						int blockedLane = opponentStatus.getLane();
						int newLane = this.randomLane(blockedLane, maxLane);
						log.debug("Got lane "+newLane+" for overtaking");
						String command = this.myCarStatus.getLane() > newLane ? "Left" : "Right";
						this.opponentLaneSwitch = new AnalyzationResult(AnalyzationMode.LANE_SWITCH, command);
						break;
					}
				}
			}
		}
	}
	
	private int randomLane(int blockedLane, int maxLane) {
		if (maxLane <= 0) {
			return 0;
		}
		Random rand = new Random();
		int randomLane;
		do {
			randomLane = rand.nextInt(maxLane+1);
		} while (randomLane == blockedLane);
		return randomLane;
	}
	
	private void adjustBendSpeed() {
		if (this.prevStatus == null || this.myCarStatus == null) {
			return;
		}
		AnalyzedPiece prevPiece = this.track.getAnalyzedPiece(this.prevStatus.getPieceIndex());
		AnalyzedPiece currentPiece = this.track.getAnalyzedPiece(this.myCarStatus.getPieceIndex());
		if (currentPiece.isBend()) {
			if (!prevPiece.isBend()) {
				double targetSpeed = currentPiece.getLaneSpeed(this.myCarStatus.getLane());
				if (targetSpeed > this.myCarStatus.getSpeed() + 0.75) {
					log.info("["+this.myCarStatus.getTick()+"] Came into bend "+this.myCarStatus.getPieceIndex()+" too slow ("+this.myCarStatus.getSpeed()+", "+targetSpeed+"). Not adjusting speeds");
					this.skipBendAdjust = true;
				}
			}
			if (this.bendStartIndex < 0) {
				this.bendStartIndex = this.myCarStatus.getPieceIndex();
			}
			else if (Math.abs(this.myCarStatus.getAngle()) > this.maxAngle) {
				this.maxAngle = Math.abs(this.myCarStatus.getAngle());
			}
		}
		else if (!currentPiece.isBend() && prevPiece.isBend()) {
			int bendEndIndex = this.prevStatus.getPieceIndex();
			boolean crashed = this.bendStartIndex == this.crashBendStart && this.crashBendEnd == bendEndIndex;
			double angleDiff = this.prevStatus != null ? (Math.abs(this.myCarStatus.getAngle()) - Math.abs(this.prevStatus.getAngle())) : 0d;
			log.debug("Bend "+this.bendStartIndex+"-"+bendEndIndex+" ended. maxAngle="+this.maxAngle+", angleDiff="+angleDiff+(crashed ? " (crashed)" : ""));
			if (!crashed && !this.skipBendAdjust) {
				Map<AngleAndRadius, double[]> anglesAndRadiiToSpeeds = new HashMap<>();
				double multiplier = 1d + (ANGLE_LIMIT-this.maxAngle)/(ANGLE_LIMIT*5*(angleDiff > ANGLE_DIFF_LIMIT ? 2 : 1));
				log.debug("Adjusting bends "+this.bendStartIndex+" to "+bendEndIndex+" speeds with "+multiplier);
				int bendPieces;
				List<AnalyzedPiece> pieces = this.track.getAnalyzedPieces();
				if (bendEndIndex < this.bendStartIndex) {
					bendPieces = pieces.size() - this.bendStartIndex + bendEndIndex + 1;
				}
				else {
					bendPieces =  bendEndIndex - this.bendStartIndex + 1;
				}
				
				for (int i=0; i<bendPieces; ++i) {
					int index = i + this.bendStartIndex;
					if (index >= pieces.size()) {
						index -= pieces.size();
					}
					
					AnalyzedPiece piece = pieces.get(index);
					if (this.maxAngle < ANGLE_LIMIT && !(angleDiff > ANGLE_DIFF_LIMIT && this.maxAngle > ANGLE_LIMIT/2)) {
						piece.adjustBendSpeed(multiplier);
						anglesAndRadiiToSpeeds.put(new AngleAndRadius(piece.getAngle(), piece.getRadius()), piece.getLaneSpeeds());
					}
					else {
						piece.lockSpeed();
					}
				}
				if (this.maxAngle < ANGLE_LIMIT) {
					this.adjustSimilarBends(anglesAndRadiiToSpeeds, bendPieces);
				}
			}
			this.skipBendAdjust = false;
			this.bendStartIndex = -1;
			this.maxAngle = 0;
		}
	}
	
	private void adjustBendSpeedAfterCrash() {
		if (this.bendStartIndex < 0) {
			log.error("Crashed on a straight! WTF??");
			return;
		}
		double totalBendDistance = 0;
		double traveledBendDistance = 0;
		int bendEndIndex = this.bendStartIndex;
		
		List<AnalyzedPiece> pieces = this.track.getAnalyzedPieces();
		int bendPiecesTraveled;
		if (this.myCarStatus.getPieceIndex() < this.bendStartIndex) {
			bendPiecesTraveled = pieces.size() - this.bendStartIndex + this.myCarStatus.getPieceIndex();
		}
		else {
			bendPiecesTraveled = this.myCarStatus.getPieceIndex() - this.bendStartIndex;
		}
		for (int i=0; i<pieces.size(); ++i) {
			int index = i + this.bendStartIndex;
			if (index >= pieces.size()) {
				index -= pieces.size();
			}
			AnalyzedPiece piece = pieces.get(index);
			if (!piece.isBend()) {
				bendEndIndex = index-1;
				if (bendEndIndex < 0) {
					bendEndIndex = pieces.size() - 1;
				}
				break;
			}
			
			if (i < bendPiecesTraveled) {
				traveledBendDistance += piece.getLaneLength(this.myCarStatus.getLane());
			}
			else if (i == bendPiecesTraveled) {
				traveledBendDistance += this.myCarStatus.getPieceDistance();
			}
			totalBendDistance += piece.getLaneLength(this.myCarStatus.getLane());
		}
		
		double multiplier = 1d - (totalBendDistance-traveledBendDistance)/(totalBendDistance*3);
		log.debug("Adjusting bends "+this.bendStartIndex+" to "+bendEndIndex+" speeds with "+multiplier);
		int bendPieces;
		if (bendEndIndex < this.bendStartIndex) {
			bendPieces = pieces.size() - this.bendStartIndex + bendEndIndex + 1;
		}
		else {
			bendPieces =  bendEndIndex - this.bendStartIndex + 1;
		}
		Map<AngleAndRadius, double[]> anglesAndRadiiToSpeeds = new HashMap<>();
		for (int i=0; i<bendPieces; ++i) {
			int index = i + this.bendStartIndex;
			if (index >= pieces.size()) {
				index -= pieces.size();
			}
			
			AnalyzedPiece piece = pieces.get(index);
			piece.releaseSpeed();
			piece.adjustBendSpeed(multiplier);
			anglesAndRadiiToSpeeds.put(new AngleAndRadius(piece.getAngle(), piece.getRadius()), piece.getLaneSpeeds());
		}
		this.adjustSimilarBends(anglesAndRadiiToSpeeds, bendPieces);
		this.crashBendStart = this.bendStartIndex;
		this.crashBendEnd = bendEndIndex;
	}
	
	private void adjustSimilarBends(Map<AngleAndRadius, double[]> anglesAndRadiiToSpeeds, int bendSize) {
		List<AnalyzedPiece> pieces = this.track.getAnalyzedPieces();
		int currentBendStartIndex = -1;
		for (int i=1; i<pieces.size(); ++i) {
			int index = i + this.myCarStatus.getPieceIndex();
			if (index >= pieces.size()) {
				index -= pieces.size();
			}
			
			AnalyzedPiece piece = pieces.get(index);
			if (piece.isBend()) {
				if (currentBendStartIndex < 0) {
					currentBendStartIndex = index;
				}
			}
			else if (currentBendStartIndex > 0) {
				int currentBendEndIndex = index - 1;
				int bendPieces;
				if (currentBendEndIndex < currentBendStartIndex) {
					bendPieces = pieces.size() - currentBendStartIndex + currentBendEndIndex + 1;
				}
				else {
					bendPieces =  currentBendEndIndex - currentBendStartIndex + 1;
				}
				log.debug("Found bend "+currentBendStartIndex+"-"+currentBendEndIndex+
						" (bendSize="+bendSize+", bendPieces="+bendPieces+")");
				if (Math.abs(bendPieces - bendSize) <= 1) {
					for (int j=0; j<bendPieces; ++j) {
						int adjustIndex = j + currentBendStartIndex;
						if (adjustIndex >= pieces.size()) {
							adjustIndex -= pieces.size();
						}
						AnalyzedPiece adjustPiece = pieces.get(adjustIndex);
						for (AngleAndRadius aar : anglesAndRadiiToSpeeds.keySet()) {
							if (isSimilarBend(adjustPiece, aar)) {
								log.debug("Adjusting similar bend "+adjustIndex);
								double[] speeds = anglesAndRadiiToSpeeds.get(aar);
								piece.adjustToAverage(speeds);
							}
						}
					}
				}
				currentBendStartIndex = -1;
			}
		}
	}
	
	private static boolean isSimilarBend(AnalyzedPiece piece, AngleAndRadius angleAndRadius) {
		return Math.abs(piece.getAngle()-angleAndRadius.getAngle()) < 5d && Math.abs(piece.getRadius()-angleAndRadius.getRadius()) < 7d;
	}
	
	private double resolveThrottle() {
		AnalyzedPiece currentPiece = this.track.getAnalyzedPiece(this.myCarStatus.getPieceIndex());
		List<AnalyzedPiece> pieces = this.track.getAnalyzedPieces();
		double distance = currentPiece.getLaneLength(this.myCarStatus.getLane())-this.myCarStatus.getPieceDistance();
		int nextStraightStart = -1;
		int nextStraightEnd = -1;
		double nextStraightLength = 0;
		double throttle = -1;
		boolean lastLap = this.currentLap >= this.race.getRaceSession().getLaps();
		for (int i=1; i<pieces.size(); ++i) {
			boolean nextLapPiece = false;
			int index = i + this.myCarStatus.getPieceIndex();
			if (index >= pieces.size()) {
				index -= pieces.size();
				nextLapPiece = true;
			}
			
			AnalyzedPiece piece = pieces.get(index);
			if (throttle < 0 && !(lastLap && nextLapPiece)) {
				if (piece.isBend() && distance < piece.getBrakingDistance(this.friction, this.myCarStatus.getSpeed())) {
					double bendSpeed = piece.getLaneSpeed(this.myCarStatus.getLane(), this.friction);
					log.debug("Braking for bend "+index+", bs="+bendSpeed+", mcs="+this.myCarStatus.getSpeed());
					if (bendSpeed > this.myCarStatus.getSpeed()) {
						throttle = Math.min(1d, bendSpeed/((this.turboOn ? this.turboFactor : 1)*10d));
					}
					else {
						throttle = 0d;
					}
				}
				else {
					distance += piece.getLaneLength(this.myCarStatus.getLane());
				}
			}
			
			if (this.turboTicks > 0 && !this.sendTurbo && nextStraightEnd < 0) {
				if (!piece.isBend()) {
					if (nextStraightStart < 0) {
						nextStraightStart = index;
						nextStraightLength = piece.getLength();
					}
					else {
						nextStraightLength += piece.getLength();
					}
				}
				else if (nextStraightStart > 0) {
					nextStraightEnd = index;
				}
			}
		}
		
		if (nextStraightEnd < nextStraightStart && lastLap) {
			nextStraightLength = Math.max(TURBO_STRAIGHT_LENGTH, nextStraightLength);
		}
		
		if (throttle >= 0) {
			return throttle;
		}
		
		double angleAbs = Math.abs(this.myCarStatus.getAngle());
		double angleDiff = this.prevStatus != null ? (angleAbs - Math.abs(this.prevStatus.getAngle())) : 0d;
		
		if (currentPiece.isBend()) {
			double bendSpeed = currentPiece.getLaneSpeed(this.myCarStatus.getLane(), this.friction);
			if (angleDiff < 0) {
				if (nextStraightLength >= TURBO_STRAIGHT_LENGTH && nextStraightStart >= this.myCarStatus.getPieceIndex() 
						&& nextStraightStart - this.myCarStatus.getPieceIndex() <= 1) {
					log.debug("nextStraight="+nextStraightStart+", length="+nextStraightLength);
					this.sendTurbo = true;
				}
				return 1d;
			}
			if (angleAbs > ANGLE_LIMIT || (angleAbs > ANGLE_DIFF_ANGLE_LIMIT && angleDiff > ANGLE_DIFF_LIMIT)) {
				return 0d;
			}
			if (angleDiff >= 2.5*ANGLE_DIFF_LIMIT) {
				bendSpeed = bendSpeed/4;
			}
			else if (angleDiff >= 2*ANGLE_DIFF_LIMIT) {
				bendSpeed = bendSpeed/2;
			}
//			if (this.isRaceOn() && this.myCarStatus.getSpeed() > bendSpeed) {
//				return 0d;
//			}
			if (bendSpeed > this.myCarStatus.getSpeed() + 1) {
				return 1d;
			}
			return Math.min(1d, bendSpeed/((this.turboOn ? this.turboFactor : 1)*10d));
		}
		
		if (angleDiff >=0 && (angleAbs > ANGLE_LIMIT || (angleAbs > ANGLE_DIFF_ANGLE_LIMIT && angleDiff > ANGLE_DIFF_LIMIT))) {
			return 0d;
		}
		if (nextStraightLength >= TURBO_STRAIGHT_LENGTH && nextStraightStart >= this.myCarStatus.getPieceIndex() 
				&& nextStraightStart - this.myCarStatus.getPieceIndex() <= 1) {
			log.debug("nextStraight="+nextStraightStart+", length="+nextStraightLength);
			this.sendTurbo = true;
		}
		return 1d;
	}
	
	private boolean statusChanged(CarStatus oldStatus, CarStatus newStatus) {
		if (oldStatus == null || newStatus == null) {
			return true;
		}
		return Math.abs(oldStatus.getAngle() - newStatus.getAngle()) > EPSILON 
				|| Math.abs(oldStatus.getSpeed() - newStatus.getSpeed()) > EPSILON 
				|| oldStatus.getLane() != newStatus.getLane()
				|| oldStatus.getPieceIndex() != newStatus.getPieceIndex();
	}
	
	private CarPosition findMyPosition(CarPositions positions) {
		for (CarPosition position : positions.getData()) {
			CarId id = position.getId();
			if (this.myCarColor.equals(id.getColor())) {
				return position;
			}
		}
		return null;
	}
	
	protected boolean isRaceOn() {
		if (this.race != null && this.race.getRaceSession() != null) {
			return !this.race.getRaceSession().isQuickRace() && this.race.getRaceSession().getLaps() > 0;
		}
		return false;
	}

	private void send(final SendMsg msg) {
		writer.println(msg.toJson());
		writer.flush();
	}

}
