package tjamtjam.hwo;

import java.util.Arrays;
import java.util.List;

import tjamtjam.hwo.msg.Track.Lane;
import tjamtjam.hwo.msg.Track.TrackPiece;

public class AnalyzedPiece extends TrackPiece {

	private static final double STARTING_BEND_SPEED_MULTIPLIER = 2.75;
	private static double brakingDistanceMultiplier = 1/3.6;
	
	private double[] laneLengths;
	private double[] laneRadii;
	private double[] laneSpeeds;
	private boolean speedLocked;
	
	public AnalyzedPiece(TrackPiece piece, List<Lane> lanes) {
		this.laneLengths = new double[lanes.size()];
		this.laneRadii = new double[lanes.size()];
		this.setAngle(piece.getAngle());
		this.setLength(piece.getLength());
		this.setRadius(piece.getRadius());
		this.setSwitchPiece(piece.isSwitchPiece());
		
		if (this.isBend()) {
			for (Lane lane : lanes) {
				double multiplier = lane.getDistanceFromCenter()*this.getAngle() >= 0 ? -1 : 1;
				this.laneRadii[lane.getIndex()] = this.getRadius()+multiplier*Math.abs(lane.getDistanceFromCenter());
				this.laneLengths[lane.getIndex()] = Math.abs(this.laneRadii[lane.getIndex()]*Math.PI*this.getAngle()/180d);
			}
		}
		else {
			for (Lane lane : lanes) {
				this.laneLengths[lane.getIndex()] = this.getLength();
			}
		}
	}
	
	public double getLaneLength(int index) {
		return this.laneLengths[index];
	}
	
	public double getLaneRadius(int index) {
		return this.laneRadii[index];
	}
	
	public double[] getLaneSpeeds() {
		if (this.laneSpeeds == null) {
			return null;
		}
		return Arrays.copyOf(this.laneSpeeds, this.laneSpeeds.length);
	}
	
	public double getLaneSpeed(int index, double friction) {
		if (this.laneSpeeds == null) {
			this.computeSpeeds(friction);
		}
		return this.laneSpeeds[index];
	}
	
	public double getLaneSpeed(int index) {
		if (this.laneSpeeds == null) {
			return 1.0;
		}
		return this.laneSpeeds[index];
	}
	
	public double getBrakingDistance(double friction, double currentSpeed) {
		if (this.laneSpeeds == null) {
			this.computeSpeeds(friction);
		}
		double minSpeed = Double.MAX_VALUE;
		for (double speed : this.laneSpeeds) {
			if (speed < minSpeed) {
				minSpeed = speed;
			}
		}
		
		return brakingDistanceMultiplier*(currentSpeed*currentSpeed - minSpeed*minSpeed)/(2*RacingBot.GRAVITY*friction);
	}
	
	public static void adjustBrakingDistanceMultiplier(double multiplier) {
		brakingDistanceMultiplier *= multiplier;
	}
	
	public void adjustBendSpeed(double multiplier) {
		if (this.laneSpeeds != null && !this.speedLocked) {
			for (int i=0; i<this.laneSpeeds.length; ++i) {
				this.laneSpeeds[i] *= multiplier;
			}
		}
	}
	
	public void adjustToAverage(double[] speeds) {
		if (this.laneSpeeds != null && speeds != null && !this.speedLocked) {
			for (int i=0; i<laneSpeeds.length; ++i) {
				this.laneSpeeds[i] = (this.laneSpeeds[i] + speeds[i])/2;
			}
		}
	}
	
	private void computeSpeeds(double friction) {
		this.laneSpeeds = new double[this.laneLengths.length];
		if (this.isBend()) {
			for (int i=0; i<this.laneSpeeds.length; ++i) {
//				double magicConstant;
//				if (this.getLaneLength(i) < 50) {
//					magicConstant = 3.35;
//				}
//				else {
//					magicConstant = 3.21;
//				}
				this.laneSpeeds[i] = STARTING_BEND_SPEED_MULTIPLIER*Math.sqrt(friction*this.getLaneRadius(i)*RacingBot.GRAVITY);
			}
		}
		else {
			for (int i=0; i<this.laneSpeeds.length; ++i) {
				this.laneSpeeds[i] = 1d;
			}
		}
	}
	
	public void lockSpeed() {
		this.speedLocked = true;
	}
	
	public void releaseSpeed() {
		this.speedLocked = false;
	}

	@Override
	public String toString() {
		return "AnalyzedPiece [laneLengths=" + Arrays.toString(laneLengths)
				+ ", getLength()=" + getLength() + ", getRadius()="
				+ getRadius() + ", getAngle()=" + getAngle()
				+ ", isSwitchPiece()=" + isSwitchPiece() + ", isBend()="
				+ isBend() + "]";
	}
}
